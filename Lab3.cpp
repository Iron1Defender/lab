# include <iostream>
#include <vector>
#include <iterator>

using namespace std;

const int MIN_ROW_COUNT = 5;
const int MAX_ROW_COUNT = 30;

const int MIN_COLUMN_COUNT = 5;
const int MAX_COLUMN_COUNT = 30;

const int MIN_MINE_COUNT = 1;
const int MAX_MINE_COUNT = 800;

class MinesweeperCell //класс
{
private:
    int row;
    int column;
    string state = {"closed"};
    bool mined;
    int counter;


public:

    MinesweeperCell(int Row, int Column) //эти параметры мы передадим при создании объекта в main
    {
        this->row = Row;
        this->column = Column;
        this->state = {"closed"};
        this->mined = 0;
        this->counter = 0;
    }
    MinesweeperCell(){}

    void open()
    {
        cout << "open normal cell \n";
    }
};


/*_____________________ LABA 3 _____________________*/

class Bomb : virtual public MinesweeperCell{
private:
    string type = {"bomb"};
    int damage = 1 + rand()%3;

public:
    Bomb(int Row, int Column): MinesweeperCell(Row, Column){}
    int open()
    {
        return damage;
    }


};

class Health : virtual public MinesweeperCell{
private:
    string type = {"health"};
    int health = 1 + rand()%2;

public:
    Health(int Row, int Column): MinesweeperCell(Row, Column){}
    int open()
    {
        return health;
    }

};

class Joke : virtual public MinesweeperCell{
private:
    string type = {"joke"};
    string anekdot = {"kolobok"};

public:
    Joke(int Row, int Column): MinesweeperCell(Row, Column){}
    void open()
    {
        cout << anekdot << endl;
    }

};

class TestClass
{
private:
    int a;
    int b;
    int test;
    int health = 3;
public:
    TestClass(int A, int B, int Test)
    {
        a = A;
        b = B;
        if (0 < Test && Test < 4){
        test = Test;
        }
        else {
            test = rand() % 3 + 1;
        }
    }

    void openCell()
    {
        if (test == 3){
            Joke v(a,b);
            v.open();
        }
        else if (test == 2){
            Health v(a,b);
            cout << endl << "+ health: " << v.open() << endl;
            health += v.open();
            cout << "new health: " << health << endl;
        }
        else{
            Bomb v(a,b);
            cout << endl << "- health: " << v.open() << endl;
            health -= v.open();
            cout << "new health: " << health << endl;
        }
    }
};

/*__________________________________________________*/


class MinesweeperModel
{
private:
    int rowCount;
    int columnCount;
    int mineCount;
public:
    MinesweeperModel(int RowCount = 15, int ColumnCount = 15, int MineCount = 15)
    {
        if(MIN_ROW_COUNT < rowCount < MAX_ROW_COUNT + 1) rowCount = RowCount;

        if(MIN_COLUMN_COUNT < columnCount < MAX_COLUMN_COUNT + 1) columnCount = ColumnCount;

        if(MIN_MINE_COUNT < rowCount < MAX_MINE_COUNT + 1) mineCount = MineCount;

        else mineCount = rowCount * columnCount - 1;

        bool firstStep = 1;
        bool gameOver = 0;
        vector<MinesweeperCell> cellsTable;

        for (int row = 0;row < rowCount;++row)
        {
            vector<MinesweeperCell> cellsRow;
            for (int column = 0; column < columnCount; ++column)
            {
                cellsRow.insert(cellsRow.end(),(MinesweeperCell(row, column)));
            }
            cellsTable.insert(cellsTable.end(), cellsRow.begin(), cellsRow.end());
            cellsRow.clear();
        }
        cout << endl << "Строчки: " << rowCount << " Колонки: " << columnCount << " Мины: " <<mineCount;//ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        cout << endl << "Количество ячеек: " << cellsTable.size();//ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    }
};

int main()
{
    MinesweeperCell norm(2,5);
    TestClass obj3(3, 4, 3);
    TestClass obj2(5, 6, 2);
    TestClass obj1(2, 2, 1);

    norm.open();

    cout << endl;

    obj3.openCell();
    obj2.openCell();
    obj1.openCell();



    return 0;
}