import random

#обмеження на бомби та клітинки#
MIN_ROW_COUNT = 5
MAX_ROW_COUNT = 30

MIN_COLUMN_COUNT = 5
MAX_COLUMN_COUNT = 30

MIN_MINE_COUNT = 1
MAX_MINE_COUNT = 800
#################################


class MinesweeperCell:

    def __init__( self, row, column ):
        self.row = row
        self.column = column
        self.state = 'closed'
        self.mined = False
        self.counter = 0


#_______________________LAB 2_______________________#


class Bomb(MinesweeperCell):
    type = "bomb"
    damage = random.randint(1, 3)
    def print(self):#ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        print("9.11:", self.type, self.damage, "--состояние:", self.state, "--координаты", self.row, self.column)


class Health(MinesweeperCell):
    type = "health"
    health = random.randint(1, 2)
    def print(self):#ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        print("Сейчас подлатаем:", self.type, self.health, "--состояние:", self.state, "--координаты", self.row, self.column)


class Joke(MinesweeperCell):
    type = "joke"
    def print(self): #ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        print("Взрывная шутка:", self.type, "--состояние:", self.state, "--координаты", self.row, self.column)



######################################################


class MinesweeperModel:
    def __init__( self, rowCount, columnCount, mineCount):
        self.startGame(rowCount, columnCount, mineCount)

    def startGame( self, rowCount, columnCount, mineCount):
        if rowCount in range( MIN_ROW_COUNT, MAX_ROW_COUNT + 1 ):
            self.rowCount = rowCount

        if columnCount in range( MIN_COLUMN_COUNT, MAX_COLUMN_COUNT + 1 ):
            self.columnCount = columnCount

        if mineCount < self.rowCount * self.columnCount:
            if mineCount in range( MIN_MINE_COUNT, MAX_MINE_COUNT + 1 ):
                self.mineCount = mineCount
        else:
            self.mineCount = self.rowCount * self.columnCount - 1

        self.firstStep = True
        self.gameOver = False
        self.cellsTable = []
        for row in range( self.rowCount ):
            cellsRow = []
            for column in range( self.columnCount ):
                cellsRow.append( MinesweeperCell( row, column ) )
            self.cellsTable.append( cellsRow )


#ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
obj1 = Bomb(3, 5)
obj2 = Health(2, 5)
obj3 = Joke(4, 1)

obj1.print()
obj2.print()
obj3.print()
#ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!