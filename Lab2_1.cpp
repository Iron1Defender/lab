# include <iostream>
#include <vector>
#include <iterator>

using namespace std;

const int MIN_ROW_COUNT = 5;
const int MAX_ROW_COUNT = 30;

const int MIN_COLUMN_COUNT = 5;
const int MAX_COLUMN_COUNT = 30;

const int MIN_MINE_COUNT = 1;
const int MAX_MINE_COUNT = 800;

class MinesweeperCell //класс
{
private:
    int row;
    int column;
    string state = {"closed"};
    bool mined;
    int counter;


public:

    MinesweeperCell(int Row, int Column) //эти параметры мы передадим при создании объекта в main
    {
        this->row = Row;
        this->column = Column;
        this->state = {"closed"};
        this->mined = 0;
        this->counter = 0;
        cout << "Ячейка готова!"; //ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
    MinesweeperCell(){}
};


/*_____________________ LABA 2 _____________________*/

class Bomb : virtual public MinesweeperCell{
private:
    string type = {"bomb"};
    int damage = 1 + rand()%3;

public:
    Bomb(int Row, int Column): MinesweeperCell(Row, Column){}
    void Print()
    {
        cout << "\n" << "type = " << type << "\n" << "damage = " << damage << endl;
    }

};

class Health : virtual public MinesweeperCell{
private:
    string type = {"health"};
    int health = 1 + rand()%2;

public:
    Health(int Row, int Column): MinesweeperCell(Row, Column){}
    void Print()
    {
        cout << "\n" << "type = " << type << "\n" << "health = " << health << endl;
    }

};

class Joke : virtual public MinesweeperCell{
private:
    string type = {"joke"};

public:
    Joke(int Row, int Column): MinesweeperCell(Row, Column){}
    void Print()
    {
        cout << "\nВзрывная шутка";
    }

};
/*__________________________________________________*/


class MinesweeperModel
{
private:
    int rowCount;
    int columnCount;
    int mineCount;
public:
    MinesweeperModel(int RowCount = 15, int ColumnCount = 15, int MineCount = 15)
    {
        if(MIN_ROW_COUNT < rowCount < MAX_ROW_COUNT + 1) rowCount = RowCount;

        if(MIN_COLUMN_COUNT < columnCount < MAX_COLUMN_COUNT + 1) columnCount = ColumnCount;

        if(MIN_MINE_COUNT < rowCount < MAX_MINE_COUNT + 1) mineCount = MineCount;

        else mineCount = rowCount * columnCount - 1;

        bool firstStep = 1;
        bool gameOver = 0;
        vector<MinesweeperCell> cellsTable;

        for (int row = 0;row < rowCount;++row)
        {
            vector<MinesweeperCell> cellsRow;
            for (int column = 0; column < columnCount; ++column)
            {
                cellsRow.insert(cellsRow.end(),(MinesweeperCell(row, column)));
            }
            cellsTable.insert(cellsTable.end(), cellsRow.begin(), cellsRow.end());
            cellsRow.clear();
        }
        cout << endl << "Строчки: " << rowCount << " Колонки: " << columnCount << " Мины: " <<mineCount;//ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        cout << endl << "Количество ячеек: " << cellsTable.size();//ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    }
    ~MinesweeperModel()
    {
        cellsTable.clear();
    }
};

int main()
{
    Bomb obj1(1,3);
    obj1.Print();

    cout << endl;

    Health obj2(3,2);
    obj2.Print();

    cout << endl;

    Joke obj3(5,5);
    obj3.Print();

    cout << endl;


    return 0;
}