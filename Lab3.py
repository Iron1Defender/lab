import random

MIN_ROW_COUNT = 5
MAX_ROW_COUNT = 30

MIN_COLUMN_COUNT = 5
MAX_COLUMN_COUNT = 30

MIN_MINE_COUNT = 1
MAX_MINE_COUNT = 800



class MinesweeperCell:

    def __init__( self, row, column ):
        self.row = row
        self.column = column
        self.state = 'closed'
        self.mined = False
        self.counter = 0

    def open(self):
        pass



class Bomb(MinesweeperCell):
    type = "bomb"
    damage = random.randint(1, 3)

    def open(self):
        if MinesweeperModel.health - self.damage > 0:
            MinesweeperModel.health = MinesweeperModel.health - self.damage
            print(self.row, self.column)  # TEST
        else:
            MinesweeperModel.isGameOver(self)
            MinesweeperModel.health = MinesweeperModel.health - self.damage
            print(self.row, self.column) #TEST
        print("type: bomb") #TEST



class Health(MinesweeperCell):
    type = "health"
    health = random.randint(1, 2)

    def open(self):
        MinesweeperModel.health = MinesweeperModel.health + self.health
        print(self.row, self.column) #TEST
        print("type: health") #TEST



class Joke(MinesweeperCell):
    type = "joke"
    anekdot = "Kolobok"

    def open(self):
        print("type: joke") #TEST
        print(self.row, self.column) #TEST
        print(self.anekdot) #TEST
        return self.anekdot



class GenerateMines:
    def __init__(self, numberMine, rowCount, columnCount, typeCell):
        self.counter = numberMine
        self.rowCount = rowCount
        self.columnCount = columnCount
        self.typeCell = typeCell

    def generator(self):
        for i in range(self.counter):
            while True:
                self.x = random.randint(0, self.rowCount - 1)
                self.y = random.randint(0, self.columnCount - 1)
                cell = MinesweeperModel.cellsTable[self.x][self.y]
                if not cell.state == 'opened' and not cell.mined:
                    if self.typeCell == "bomb":
                        cell = Bomb(self.x, self.y)
                        cell.mined = True
                        cell.open()
                    elif self.typeCell == "health":
                        cell = Health(self.x, self.y)
                        cell.mined = True
                        cell.open()
                    else:
                        cell = Joke(self.x, self.y)
                        cell.mined = True
                        cell.open() #TEST
                    print("player health: ", MinesweeperModel.health, "\n") #TEST
                    MinesweeperModel.cellsTable[self.x][self.y] = cell
                    break



class MinesweeperModel:
    health = 3  # здоровье игрока
    cellsTable = []
    def __init__( self, rowCount, columnCount, bombCount, healthCount, jokeCount):
        self.startGame(rowCount, columnCount, bombCount+healthCount+jokeCount, bombCount, healthCount, jokeCount)
    def startGame( self, rowCount, columnCount, mineCount, bombCount, healthCount, jokeCount):
        if rowCount in range( MIN_ROW_COUNT, MAX_ROW_COUNT + 1 ):
            self.rowCount = rowCount

        if columnCount in range( MIN_COLUMN_COUNT, MAX_COLUMN_COUNT + 1 ):
            self.columnCount = columnCount

        if mineCount < self.rowCount * self.columnCount:
            if mineCount in range( MIN_MINE_COUNT, MAX_MINE_COUNT + 1 ):
                self.mineCount = mineCount
                self.bombCount = bombCount
                self.healthCount = healthCount
                self.jokeCount = jokeCount
        else: #ДОРОБИТИ
            self.mineCount = self.rowCount * self.columnCount - 1

        self.firstStep = True
        self.gameOver = False
        for row in range( self.rowCount ):
            cellsRow = []
            for column in range( self.columnCount ):
                cellsRow.append( MinesweeperCell( row, column ) )
            self.cellsTable.append( cellsRow )



    def isGameOver( self ):
        print("In this part you lose") #TEST
        #return self.gameOver

    def getCell( self, row, column ):
        a = self.cellsTable[row][column] #TEST
        print(a.mined) #TEST

        return self.cellsTable[row][column]

    def openCell( self, row, column ):
        cell = self.getCell( row, column )
        if not cell:
            return

        if self.firstStep:
            self.firstStep = False
            self.generateMines()



    def generateMines( self ):
        bomb = GenerateMines(self.bombCount, self.rowCount, self.columnCount, "bomb")
        health = GenerateMines(self.healthCount, self.rowCount, self.columnCount, "health")
        joke = GenerateMines(self.jokeCount, self.rowCount, self.columnCount, "joke")

        bomb.generator()
        health.generator()
        joke.generator()

#ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
a = MinesweeperModel(6, 8, 3, 2, 1) #x, y, бомбы, аптечки, шутки
a.openCell(5,2)


#obj1.openCell()
#ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!