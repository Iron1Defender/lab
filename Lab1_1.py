
#обмеження на бомби та клітинки#
MIN_ROW_COUNT = 5
MAX_ROW_COUNT = 30

MIN_COLUMN_COUNT = 5
MAX_COLUMN_COUNT = 30

MIN_MINE_COUNT = 1
MAX_MINE_COUNT = 800
#################################
class MinesweeperCell:

    def __init__( self, row, column ):
        self.row = row
        self.column = column
        self.state = 'closed'
        self.mined = False
        self.counter = 0
        print("Ячейка готова!", end=" ") #ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

class MinesweeperModel:
    def __init__( self, rowCount, columnCount, mineCount):
        self.startGame(rowCount, columnCount, mineCount)

    def startGame( self, rowCount, columnCount, mineCount):
        if rowCount in range( MIN_ROW_COUNT, MAX_ROW_COUNT + 1 ):
            self.rowCount = rowCount

        if columnCount in range( MIN_COLUMN_COUNT, MAX_COLUMN_COUNT + 1 ):
            self.columnCount = columnCount

        if mineCount < self.rowCount * self.columnCount:
            if mineCount in range( MIN_MINE_COUNT, MAX_MINE_COUNT + 1 ):
                self.mineCount = mineCount
        else:
            self.mineCount = self.rowCount * self.columnCount - 1

        self.firstStep = True
        self.gameOver = False
        self.cellsTable = []
        i = 0#ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        for row in range( self.rowCount ):
            cellsRow = []
            for column in range( self.columnCount ):
                cellsRow.append( MinesweeperCell( row, column ) )
                i += 1 #ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            self.cellsTable.append( cellsRow )
        print("\n", "Длина: ", i, "\n", "Строчки: ", rowCount," Колонки: ", columnCount, " Мины: ", mineCount) #ДЛЯ ТЕСТА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

obj1 = MinesweeperCell(3, 6)
obj2 = MinesweeperModel(8, 6, 12)
